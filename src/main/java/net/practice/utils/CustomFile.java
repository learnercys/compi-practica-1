package net.practice.utils;

import java.io.*;
import java.net.URL;

/**
 * @author learnercys
 * Created on 3/1/15.
 */
public class CustomFile extends File {

    private String extension;

	public CustomFile ( URL url ) {
		this( url.getPath() );
	}

    public CustomFile ( String pathName ) {
        super( pathName );
        this.extension = super.getName().substring( super.getName().indexOf(".") + 1, super.getName().length() );
    }

    public CustomFile ( File file ) {
        this( file.getAbsolutePath() );
    }

    public String readFile(  ) {
        return this.readFile( this );
    }

    public String readFile ( File file ) {
        StringBuilder stringBuilder = new StringBuilder( );
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader( new FileReader( file ) );
            String text;
            String newLine = "";
            while( (text = bufferedReader.readLine()) != null ) {
                stringBuilder.append(newLine+ text );
                newLine = "\n";
            }
        }  catch( IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch ( IOException ex ) {
                ex.printStackTrace();
            }
        }
        return stringBuilder.toString();
    }

    public void saveFile ( String newText ) {
        try {
            PrintWriter printWriter = new PrintWriter( this.getAbsolutePath() );
            String [] lines = newText.split("\n");
            for(String line: lines) {
                printWriter.println( line );
            }
            printWriter.close();
        } catch (FileNotFoundException ex ) {
            // do nothing. something really rare happened.
        }
    }

    public String getExtension( ) {
        return this.extension;
    }

    public static String getExtension ( File file ) throws NullPointerException {
        if( file.getName().lastIndexOf( "." ) < 0 ) {
            return null;
        }
        return file.getName().substring( file.getName().lastIndexOf( "." ) + 1, file.getName().length() );
    }

	public static boolean createDirectory ( String dir ) {
		return new File(dir).mkdirs();
	}

	public static boolean createFile ( String f ) {
		try {
			return new File( f ).createNewFile();
		} catch ( IOException e ) {
			return false;
		}
	}

    public void setExtension ( String extension ) {
        this.extension = extension;
    }
}
