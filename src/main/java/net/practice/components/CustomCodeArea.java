package net.practice.components;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.stage.FileChooser;
import net.practice.Main;
import net.practice.parser.Parser;
import net.practice.scanner.Scanner;
import net.practice.utils.CustomFile;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.StyleSpans;
import org.fxmisc.richtext.StyleSpansBuilder;


/**
 * @author learnercys
 * Created on 3/1/15.
 */
public class CustomCodeArea extends CodeArea {

    // to know if we need to save the current CodeArea or not.
    private CustomFile file;

    private static final String STRUCTURE_PATTERN = "</?estructura>";
    private static final String DIRECTORY_PATTERN = "</?directorio>";
    private static final String FOLDER_PATTERN = "</?carpeta>";
    private static final String DOCUMENT_PATTERN = "</?documento>";
    private static final String NAME_PATTERN = "</?nombre>";
    private static final String FORMAT_PATTERN = "</?formato>";
    private static final String CONTENT_PATTERN = "</?contenido>";
    private static final String STRING_PATTERN = "\"([^\"\\\\]|\\\\.)*\"";

	private final String ERROS_TEMPLATE = "/errors.mustache";

    private final Pattern PATTERN = Pattern.compile(
            "(?<STRUCTURE>" + STRUCTURE_PATTERN + ")"
            + "|(?<DIRECTORY>" + DIRECTORY_PATTERN + ")"
            + "|(?<FOLDER>" + FOLDER_PATTERN + ")"
            + "|(?<DOCUMENT>" + DOCUMENT_PATTERN + ")"
            + "|(?<NAME>" + NAME_PATTERN + ")"
            + "|(?<FORMAT>" + FORMAT_PATTERN + ")"
            + "|(?<CONTENT>" + CONTENT_PATTERN + ")"
            + "|(?<STRING>" + STRING_PATTERN + ")",
		Pattern.CASE_INSENSITIVE
    );

    public CustomCodeArea ( ) {
        this( true );
    }

    public CustomCodeArea ( boolean codeLine ) {
        if( codeLine ) {
            super.setParagraphGraphicFactory( LineNumberFactory.get( this ) );
        }
    }

    private StyleSpans<Collection<String>> computeHighlighting( String text ) {
        Matcher matcher = PATTERN.matcher(text);
        int lastKwEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();

        while(matcher.find()) {
            String styleClass =
                    matcher.group("STRUCTURE") != null ? "structure" :
                            matcher.group("DIRECTORY") != null ? "directory" :
                                    matcher.group("FOLDER") != null ? "folder" :
                                            matcher.group("DOCUMENT") != null ? "document" :
                                                    matcher.group("NAME") != null ? "name" :
                                                            matcher.group("FORMAT") != null ? "format" :
                                                                    matcher.group("CONTENT") != null ? "content" :
                                                                            matcher.group("STRING") != null ? "string" :

                    null; /* never happens */ assert styleClass != null;
            spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);

        return spansBuilder.create();
    }

    public void doCompilation () {

        if( this.getText().trim().length() == 0) {
            Dialogs
                    .create()
                    .owner( this.getScene().getWindow() )
                    .title("Compilation Error")
                    .message("There is not an structure to compile")
                    .showError();
            return;
        }

        if( file == null) {
            // first we need to save the actual current file
            Action action = Dialogs
                    .create()
                    .owner( this.getScene().getWindow())
                    .title("No saved file")
                    .masthead("Compilation error")
                    .message("You need to save the current structure to continue the execution. Continue?")
                    .actions(Dialog.ACTION_OK, Dialog.ACTION_NO)
                    .showConfirm();

            if( action == Dialog.ACTION_NO ) {
                return;
            }

            if( !this.saveFile() ) {
                // if we don't save the current text, the compilation is cancelled.
                return;
            }

        } else {
            if( !this.getText().equals(file.readFile()) ) {
                // if the actual text in the code area is not equal. we cannot continue with the
                // compilation. So, we automatically save the file.
                file.saveFile( this.getText() );
            }
        }

        StringReader stringReader = new StringReader( this.getText() );
        Scanner scanner = new Scanner( stringReader );
		Parser parser = new Parser(scanner);
        try {
            this.setStyleSpans(0, computeHighlighting( this.getText() ));

            parser.parse();

			if( parser.hasUnRecoveredSyntaxError ) {
				Dialogs.create()
					.owner( this.getScene().getWindow() )
					.title("Syntax error")
					.message("The app has an unrecovered syntax error, verify your structure")
					.showInformation();
				return;
			}

			//if( scanner.hasErrors() ) {
			if( scanner.hasErrors() ) {
				// confirm errors creation
				Action response = Dialogs.create()
					.owner( this.getScene().getWindow() )
					.title("Save Errors")
					.masthead("Found errors")
					.message("Do you want to save the errors")
					.actions(Dialog.ACTION_OK, Dialog.ACTION_NO)
					.showConfirm();

				if( response == Dialog.ACTION_NO) {
					return;
				}

				// create errors
				FileChooser errorChooser = new FileChooser();
				errorChooser.getExtensionFilters().addAll(
					new FileChooser.ExtensionFilter("HTML Files", "*.html")
				);

				File errorFile = errorChooser.showSaveDialog( this.getScene().getWindow() );
				if( errorFile != null ) {
					if( null == CustomFile.getExtension( errorFile ) ) {
						errorFile = new File( errorFile.getAbsolutePath() + ".html" );
					}

					MustacheFactory mustacheFactory = new DefaultMustacheFactory();
					Mustache mustache = mustacheFactory.compile(Main.mTemplates.getPath() + this.ERROS_TEMPLATE);
					mustache.execute( new PrintWriter( errorFile ), scanner).flush();

					//new CustomFile(  errorFile ).saveFile( html );
				}
			} else {

				for(HashMap<String, String> f : parser.docs) {
					String ap = f.get("path") + "/" + f.get("name") + "." + f.get("format");
					CustomFile.createFile( ap );
					if( f.containsKey("content") ) {
						new CustomFile( ap ).saveFile( f.get("content").substring(1, f.get("content").length() - 1));
					}
				}

				// create structure
				Dialogs.create()
					.owner( this.getScene().getWindow() )
					.title( "Create structure")
					.message( "The structure was created successfully")
					.showInformation();
			}
            stringReader.close();
        } catch ( IOException ex ) {
            // do nothing
        } catch ( Exception e ) {
			// another do nothing
		}
    }

    /**
     * Create a new file with Dialogs components and then write in the actual file the
     * current CodeArea's text.
     *
     * @return The result save the new file.
     */
    public boolean saveFile () {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Cyd Files", "*.cyd")
        );
        File newFile = chooser.showSaveDialog( this.getScene().getWindow() );
        if( newFile == null ) {
            return false;
        }
        if (null == CustomFile.getExtension(newFile)) {
            newFile = new File( newFile.getAbsolutePath() + ".cyd");
        }
        this.file = new CustomFile( newFile );
        this.file.saveFile( this.getText() );
        return true;
    }

    /**
     * Indicate if the current area code is saved or not.
     *
     * When is saved?
     * 1.   if the current code area does not have some text and the file is null.
     *      Nothing exist, so we don't need to save.
     *
     * 2.   if the current code area have some text and this text is the some to the
     *      current file.
     *
     * @return really?
     */
    public boolean isSaved ()  {
        try {
            return this.getText().trim().length() == 0 && this.file == null
                    || this.getText().equals( this.file.readFile() );

        } catch ( NullPointerException ex) {
            return false;
        }
    }

    @Override
    public void replaceText (int start, int end, String text ) {
        super.replaceText( start, end, text );
    }

    public void replaceText ( String text ) {
        this.replaceText(0, this.getLength(), text);
    }

    public CustomFile getFile ( ) {
        return this.file;
    }

    public void setFile ( CustomFile file ) {
        this.file = file;
        this.replaceText( file.readFile() );
    }
}
