package net.practice;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * @author learnercys
 * Created on 3/1/15.
 */
public class Main extends Application {

    public static final String APP_TITLE = "structure generator";
	public static java.net.URL mTemplates;

    public void start ( Stage primaryStage ) throws IOException{
        BorderPane root = FXMLLoader.load( getClass().getResource( "fxml/mainctrl.fxml" ) );
		mTemplates = getClass().getResource("templates");
        primaryStage.setTitle( APP_TITLE );
        primaryStage.setScene( new Scene( root ) );
        primaryStage.show();
    }

    public static void main ( String [] args ) {
        launch( args );
    }

}
