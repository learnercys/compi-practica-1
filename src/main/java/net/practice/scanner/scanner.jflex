
package net.practice.scanner;

import java.util.ArrayList;
import java.util.HashMap;
import java_cup.runtime.*;
import net.practice.parser.*;

%%
%public
%class Scanner
%unicode
%line
%column
%ignorecase
%cupsym sym
%cup

%{
	public boolean hasErrors () {
		return errors.size() != 0;
	}

	public ArrayList<HashMap<String, String>> errors = new ArrayList<>();

	private Symbol symbol(int type) {
	  return new Symbol(type, yyline, yycolumn);
	}
	private Symbol symbol(int type, Object value) {
	  return new Symbol(type, yyline, yycolumn, value);
	}
%}

// single characters
MoreThan    =">"
LessThan    ="<"
LessThanS   ="</"
String      =[^\"]+
Quote       =[\"]
WhiteSpace  =[ \n\t\f]

// single words
SWStructure = "estructura"
SWDirectory = "directorio"
SWFolder    = "carpeta"
SWName      = "nombre"
SWDocument  = "documento"
SWFormat    = "formato"
SWContent   = "contenido"


// main tags
InitStructure = {LessThan}  {SWStructure} {MoreThan}
EndStructure  = {LessThanS} {SWStructure} {MoreThan}

InitDirectory = {LessThan}  {SWDirectory} {MoreThan}
EndDirectory  = {LessThanS} {SWDirectory} {MoreThan}

InitFolder    = {LessThan}  {SWFolder} {MoreThan}
EndFolder     = {LessThanS} {SWFolder} {MoreThan}

InitName      = {LessThan}  {SWName} {MoreThan}
EndName       = {LessThanS} {SWName} {MoreThan}

InitDocument  = {LessThan}  {SWDocument} {MoreThan}
EndDocument   = {LessThanS} {SWDocument} {MoreThan}

InitFormat    = {LessThan}  {SWFormat} {MoreThan}
EndFormat     = {LessThanS} {SWFormat} {MoreThan}

InitContent   = {LessThan}  {SWContent} {MoreThan}
EndContent    = {LessThanS} {SWContent} {MoreThan}

DirectoryAndContent = {Quote} {String} {Quote}
NameAndFormat = [a-zA-Z_][a-zA-Z0-9_]*

%%

{InitStructure} { return symbol(sym.init_str); }
{EndStructure}  { return symbol(sym.end_str); }

{InitDirectory} { return symbol(sym.init_dir); }
{EndDirectory}  { return symbol(sym.end_dir); }

{InitFolder}    { return symbol(sym.init_folder); }
{EndFolder}     { return symbol(sym.end_folder); }

{InitName}      { return symbol(sym.init_name); }
{EndName}       { return symbol(sym.end_name); }

{InitDocument}  { return symbol(sym.init_document); }
{EndDocument}   { return symbol(sym.end_document); }

{InitFormat}    { return symbol(sym.init_format); }
{EndFormat}     { return symbol(sym.end_format); }

{InitContent}   { return symbol(sym.init_content); }
{EndContent}    { return symbol(sym.end_content); }

{DirectoryAndContent} { return symbol(sym.string, yytext() ); }

{NameAndFormat} { return symbol(sym.id, yytext()); }

{WhiteSpace} { /*System.out.println("WhiteSpace");*/ }

. {
	System.out.println("Line: " + (yyline+1) + " Column: " + (yycolumn+1) + " - Lexical error in: " + yytext());
	HashMap<String, String> error = new HashMap<>();
	error.put("line", Integer.toString(yyline + 1));
    error.put("column", Integer.toString(yycolumn +1));
    error.put("text", yytext());
    error.put("number", Integer.toString(errors.size() + 1));
    errors.add( error );
}
