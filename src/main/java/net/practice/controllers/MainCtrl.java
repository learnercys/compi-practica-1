package net.practice.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import net.practice.utils.CustomFile;
import net.practice.components.CustomCodeArea;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

/**
 * @author learnercys
 * Created on 3/1/15.
 */
public class MainCtrl implements Initializable {

    @FXML private BorderPane root;
    @FXML private MenuItem autoCompilationItem;

    private boolean isAutoCompilable = false;
    private CustomCodeArea area;
    private Timer promiseArea;

    private ChangeListener<String> areaListener;


    /**
     * Close the current application
     */
    public void closeApplication ( ) {
        if( !area.isSaved() ) {
            Action response = Dialogs.create()
                    .owner( root.getScene().getWindow() )
                    .title( "No saved file" )
                    .masthead( "The current file has not saved information")
                    .message( "Do you want to save the information?")
                    .showConfirm();

            if( response == Dialog.ACTION_OK ) {
                area.getFile().saveFile( area.getText() );
            } else if ( response == Dialog.ACTION_CANCEL ) {
                return;
            }
        }
        Stage stage = (Stage)root.getScene().getWindow();
        stage.close();
    }

    /**
     * enable or disable auto compilation.
     */
    public void handleAutoCompilation ( ) {
        this.isAutoCompilable = !this.isAutoCompilable;
        if( this.isAutoCompilable ) {
            area.textProperty().addListener( areaListener );
            autoCompilationItem.setText("Remove auto-compilation");
        } else {
            area.textProperty().removeListener(areaListener);
            autoCompilationItem.setText("Add auto-compilation");
        }
    }

    /**
     * Create a new file.
     */
    public void newFile ( ) {
        if( !area.isSaved() ) {
            Action response = Dialogs
                    .create()
                    .owner( root.getScene().getWindow() )
                    .title( "No saved file")
                    .masthead("The current file has not saved information")
                    .message("Do you want to save the information?")
                    .showConfirm();

            if( response == Dialog.ACTION_OK) {
                area.getFile().saveFile( area.getText() );
            } else if( response == Dialog.ACTION_CANCEL) {
                return;
            }
        }

        area.replaceText("");
        area.saveFile();
    }

    /**
     * open an existing file.
     */
    public void openFile ( ) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Cyd Files", "*.cyd")
        );
        fileChooser.setTitle( "Open File" );
        if( !area.isSaved() ) {
            Action response = Dialogs.create()
                    .owner( root.getScene().getWindow() )
                    .title("No Saved file")
                    .masthead( "You have some changes without save.")
                    .message( "Do you want to saved first?")
                    .actions( Dialog.ACTION_OK, Dialog.ACTION_NO)
                    .showConfirm();

            if( response == Dialog.ACTION_OK) {
                this.saveFile();
            }
        }

        try {

            CustomFile file = new CustomFile( fileChooser.showOpenDialog(root.getScene().getWindow()) );
            if( file.canRead() ) {
                if( file.getExtension().equals( "cyd" ) ) {
                    area.setFile( file );
                }
            }
        } catch ( NullPointerException ex ) {
            // nothing to do
        }
    }

    /**
     * save the current file.
     */
    public void saveFile ( ) {
        if( area.getFile() == null && area.getText().trim().length() > 0 ) {
            area.saveFile();

        } else if( area.getFile() == null && area.getText().trim().length() == 0 ){
            Dialogs.create()
                    .owner( root.getScene().getWindow() )
                    .title("Nothing to save")
                    .message( "There is nothing to save")
                    .showError();

        } else {
            if( area.getText().trim().length() == 0) {
                Action response = Dialogs.create()
                        .owner( root.getScene().getWindow() )
                        .title( "Empty file" )
                        .message( "The current structure is empty. Are you sure you want to save")
                        .actions( Dialog.ACTION_OK, Dialog.ACTION_NO )
                        .showConfirm();

                if ( response == Dialog.ACTION_NO ) {
                    return;
                }
            }

            if( area.getFile().readFile().equals( area.getText())) {
                // there is nothing to save
                Dialogs.create()
                        .owner(root.getScene().getWindow())
                        .title("No file changes")
                        .message("The current file has no changes to save")
                        .showError();
            } else {
                area.getFile().saveFile( area.getText() );
                Dialogs.create().owner( root.getScene().getWindow() )
                        .title("Save Files")
                        .message("All the changes was added to the file")
                        .showInformation();
            }
        }
    }

    /**
     * do the compilation.
     * TODO show results of compilations.
     */
    public void startCompilation ( ) {
        area.doCompilation();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // initialize the Listener
        areaListener = ( obs, oldText, newText) -> {
            if( promiseArea != null ) {
                promiseArea.cancel();
            }

            promiseArea = new Timer( );
            promiseArea.schedule(new TimerTask() {
                @Override
                public void run() {
                    area.doCompilation();
                }
            }, 700);
        };

        area = new CustomCodeArea();

        if( this.isAutoCompilable ) {
            area.textProperty().addListener( areaListener );
        }

        root.setCenter( area );
    }
}
